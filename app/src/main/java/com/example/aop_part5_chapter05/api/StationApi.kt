package com.example.aop_part5_chapter05.api

import com.example.aop_part5_chapter05.db.entity.StationEntity
import com.example.aop_part5_chapter05.db.entity.SubwayEntity

interface StationApi {

    suspend fun getStationDataUpdatedTimeMillis(): Long

    suspend fun getStationSubways(): List<Pair<StationEntity, SubwayEntity>>
}